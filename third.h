#include <algorithm>
#include <iostream>
#include <stack>

using namespace std;

int main() {
  int n;
  cin >> n;
  stack<pair<long, long>> sk;
  sk.push(make_pair(0, -1));
  long i, a = 0, b = 0, x = 0, maxi = 0;
  for (i = 1; i < n + 1; ++i) {
    cin >> x;
    if (x > sk.top().second) {
      sk.push(make_pair(i, x));
    } else {
      while (x <= sk.top().second) {
        a = sk.top().first;
        b = sk.top().second;
        long len = i - a;
        maxi = max(maxi, len * b);
        sk.pop();
      }
      sk.push(make_pair(a, min(x, b)));
    }
  }
  x = 0;
  while (x <= sk.top().second) {
    a = sk.top().first;
    b = sk.top().second;
    long len = i - a;
    maxi = max(maxi, len * b);
    sk.pop();
  }
  cout << maxi;
}
