#include <iostream>
#include <algorithm>
#include "first.h"

using namespace std;

int main() {
  string st = "";
  Stack sk;
  int n;
  while (st != "exit") {
    cin >> st;
    if (st == "push") {
      cin >> n;
      sk.Push(n);
    }
    if (st == "pop") {
      sk.Pop(true);
    }
    if (st == "back") {
      sk.Back();
    }
    if (st == "size") {
      sk.Size();
    }
    if (st == "clear") {
      sk.Clear(true);
    }
  }
  cout << "bye";
  sk.Clear(false);
  return 0;
}