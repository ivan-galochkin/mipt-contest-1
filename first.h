#include <iostream>

using namespace std;

struct Stack {
  int a = 0;
  int prev_count = -1;
  Stack* prev = nullptr;

  void Push(int b);

  void Clear(bool print);

  void Back();

  void Size();

  void Pop(bool print);
};

void Stack::Pop(bool print) {
  if (prev_count == -1 && print) {
    cout << "error" << endl;
  } else {
    if (print) {
      cout << a << endl;
    }
    if (prev_count == 0) {
      prev_count = -1;
    } else {
      a = prev->a;
      Stack* temp = prev;
      prev_count = prev->prev_count;
      this->prev = this->prev->prev;
      delete temp;
    }
  }
}

void Stack::Back() {
  if (prev_count == -1) {
    cout << "error" << endl;
  } else {
    cout << a << endl;
  }
}

void Stack::Size() { cout << prev_count + 1 << endl; }

void Stack::Clear(bool print) {
  int k = prev_count;
  for (int i = 0; i < k; ++i) {


    Pop(false);
  }
  prev_count = -1;

  if (print) {
    cout << "ok" << endl;
  }
}

void Stack::Push(int b) {
  if (prev_count != -1) {
    Stack* sk = new Stack();
    sk->a = a;
    sk->prev_count = prev_count;
    if (sk->prev_count != -1) {
      sk->prev = prev;
    }
    prev = sk;
    a = b;
    prev_count = sk->prev_count + 1;
  } else {
    a = b;
    prev_count = 0;
  }
  cout << "ok" << endl;
}